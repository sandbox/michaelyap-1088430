README.txt
==========

The Joget Workflow module allows Drupal to connect to a Joget install and displays 
either an inbox listing of pending tasks and/or an available application list from 
Joget.

== Description ==

Joget Workflow (www.joget.org) is an open source application builder that helps you 
build any process-driven web application either by making it codeless or helping you 
code less. The Joget Workflow module is a Drupal module written to connect to any 
Joget install. The widget displays a list of pending tasks for a logged in user and/or 
the list of available applications created from Joget accessible to the user. Please 
note that if properly configured, your Joget installation should connect seamlessly to 
your Drupal deployment, sharing its users and roles.

For more information about Joget, please visit http://www.joget.org or sign up for our 
community site at http://dev.joget.org/community.

== Installation ==
1. Upload the Joget folder to the `/modules/` directory.
2. Activate the module through the Modules menu in the Drupal administrator.
3. Place the Joget Inbox block in a location thru the Structure > Block menu.
4. Configure the block to point to your Joget. By default, the URL should be configured 
up to your /wflow-wfweb/ (i.e., http://localhost:8080/wflow-wfweb/).
5. Make sure your Joget installation is loaded and configured with the correct Drupal 
Directory Manager plugin.

== Frequently Asked Questions ==

== What can I build with Joget Workflow ? ==
Any process-driven web application. It could be a CRM, ERP or HR management solution, or 
procurement software. If it's process-driven or process-centric, Joget will help you 
build it.

== What technology is Joget Workflow written on? ==
Joget is built mostly on a J2EE stack.

== Does that mean that it will not apply to the PHP guys in the Drupal community ? =
Absolutely not. We've designed our integration points to be web technology agnostic. You 
can do a whole lot with our JSON and REST APIs without having to ever touch the JAVA suite. 
The Drupal module itself is written purely on PHP and JavaScript.

== What if I can also do JAVA ? ==
More power to you ! We have a full OSGI implementation that will allow you to build very, 
very, very cool stuff. We have existing plugins that can connect to LDAPs or Google Apps, 
or even make it so that you can send SMS or make calls to phones from your web apps simply 
by dragging boxes around on your browser !

== The block looks weird.. ==
Feel free to change it. The joget.css is packaged with the module.

== How can I learn more about Joget Workflow ? ==
We always welcome new members to our growing community and hope that we can help you build 
cool stuff. Register and join our community of developers at http://dev.joget.org/community.

== Changelog ==

= 1.0 =
* Initial version
